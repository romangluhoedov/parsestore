<?php

ini_set("max_execution_time", 0);

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH."wp-load.php");

require_once('phpQuery.php');

class StoreParser
{

    private $siteUrl;

    public function __construct($siteUrl)
    {
        $this->siteUrl = $siteUrl;
    }

    public function parse()
    {

        echo 'Parsing ua.iherb.com...'.PHP_EOL;

        $mainpage = file_get_contents($this->siteUrl);

        $mainpageDoc = phpQuery::newDocument($mainpage);

        $productsArr = [];

        $counter = 0;

        $menu = $mainpageDoc->find('.nav-item-list li:not(.alternate) a');

        $categories = [];

        foreach ($menu as $key => $item) {
            $pq = pq($item);
            $categories[$key]['link'] = 'https:'.$pq->attr('href');
            $categories[$key]['title'] = $pq->html();
        }

        if (empty($categories)) return;
        foreach ($categories as $category) {
//            if ($category['link'] != 'https://ua.iherb.com/c/supplements') continue;

            $catSlug = explode('/c/', $category['link']);
            require_once ABSPATH . '/wp-admin/includes/taxonomy.php';

            $getTerm = get_term_by('name', $category['title'], 'product_cat');

            if (!$getTerm) {
                $insertCat = wp_insert_term($category['title'], 'product_cat',
                    array('slug' => $catSlug[1]));

                $categoryID = $insertCat['term_id'];

                if (is_wp_error($insertCat)) {
                    echo $category['title'].' - '.$insertCat->get_error_message().PHP_EOL;
                }
            }
            else {
                $categoryID = $getTerm->term_id;
            }

            // Set number of pagination pages to parse
            for ($p = 1; $p < 2; $p++) {
                $catpage = file_get_contents($category['link'].'?p='.$p);
                $catpageDoc = phpQuery::newDocument($catpage);

                $productsBlock = $catpageDoc->find('div.products.clearfix > div');

                $productLinksArr = [];

                foreach ($productsBlock as $el) {
                    $product = pq($el);
                    $productLink = $product->find('div.product-inner a');
                    $href = $productLink->attr('href');
                    $productLinksArr[] = $href;
                }

                foreach ($productLinksArr as $l) {
//                    if ($l != 'https://ua.iherb.com/pr/Solgar-Skin-Nails-Hair-Advanced-MSM-Formula-120-Tablets/22419') continue;

                    $productInfo = [];

                    $productpage = file_get_contents($l);
                    $productpageDoc = phpQuery::newDocument($productpage);
                    $productInfo['sku'] = $productpageDoc->find('ul#product-specs-list span[itemprop="sku"]')->html();

                    $productInfo['title'] = $productpageDoc->find('h1:first')->html();
                    $productInfo['category'] = $category['title'];
                    $productInfo['category_id'] = $categoryID;
                    $productSpecks = $productpageDoc->find('ul#product-specs-list li:last');
                    foreach ($productSpecks as $el) {
                        $speck = pq($el);
                        $packageQuantityString = $speck->prev()->html();
                    }

                    $packageQuantity = explode('Package Quantity:', $packageQuantityString);
                    $productInfo['package_quantity'] = trim($packageQuantity[1]);

                    $thumbs = $productpageDoc->find('div.thumbnail-container > img');
                    foreach ($thumbs as $el) {
                        $image = pq($el);
                        $src = $image->attr('src');
                        $src = preg_replace('/\/t\//', '/l/', $src); // Change to large size
                        $productInfo['images'][] = $src;
                    }

                    $price = $productpageDoc->find('div#price.price')->html();
                    $productInfo['price'] = ltrim(trim($price), '₴');

                    $stockStatus = trim($productpageDoc->find('div#stock-status .text-primary')->html());
                    $productInfo['stock_status'] = ($stockStatus == 'In Stock') ? 'instock' : 'outofstock';

                    $productInfo['brand'] = $productpageDoc->find('div#product-summary-header div#brand span[itemprop="name"] bdi:first')->html();

                    $productInfo['description'] = $productpageDoc->find('div.col-xs-24 div[itemprop="description"]')->html();

                    $productsArr[] = $productInfo;

                    $counter++;

                    echo 'Products parsed: '.$counter.PHP_EOL;

                }

            }

        }

        return $productsArr;

    }


    public function uploadProducts($products)
    {
        $counter = 0;

        $productsNum = count($products);

        echo 'Uploading products to Woocommerce...'.PHP_EOL;

        if (!empty($products)) {
            foreach ($products as $product) {

                $id = wc_get_product_id_by_sku($product['sku']);

                $id = ($id != 0) ? $id : '';

                $objProduct = new WC_Product($id);

                $objProduct->set_name($product['title']);
                $objProduct->set_status("publish");  // can be publish,draft or any wordpress product status
                $objProduct->set_catalog_visibility('visible'); // add the product visibility status
                $objProduct->set_description($product['description']);
                $objProduct->set_sku($product['sku']); //can be blank in case you don't have sku, but You can't add duplicate sku's
                $objProduct->set_price($product['price']); // set product price
                $objProduct->set_regular_price($product['price']); // set product regular price
                $objProduct->set_manage_stock(false); // true or false
                $objProduct->set_stock_status($product['stock_status']); // in stock or out of stock value
                $objProduct->set_backorders('no');
                $objProduct->set_reviews_allowed(true);
                $objProduct->set_category_ids(array($product['category_id'])); // array of category ids, You can get category id from WooCommerce Product Category Section of Wordpress Admin

                $getBrand = get_term_by('name', $product['brand'], 'product_brand');
                if (!$getBrand) {

                    $insertBrand = wp_insert_term($product['brand'], 'product_brand',
                        array('slug' => sanitize_title($product['brand'])));

                    if (is_wp_error($insertBrand)) {
                        echo $product['brand'].' - '.$insertBrand->get_error_message().PHP_EOL;
                    }
                }

                $productImagesIDs = []; // define an array to store the media ids.
                $images = $product['images']; // images url array of product
                foreach ($images as $image) {
                    $mediaID = $this->uploadMedia($image); // calling the uploadMedia function and passing image url to get the uploaded media id
                    if ($mediaID) $productImagesIDs[] = $mediaID; // storing media ids in a array.
                }
                if ($productImagesIDs) {
                    $objProduct->set_image_id($productImagesIDs[0]); // set the first image as primary image of the product

                    if (count($productImagesIDs) > 1) {
                        $gallery = array_slice($productImagesIDs, 1);
                        $objProduct->set_gallery_image_ids($gallery);
                    }
                }

                $productID = $objProduct->save();

                wp_set_object_terms( $productID, $product['brand'], 'product_brand' );

                $term_taxonomy_ids = wp_set_object_terms($productID, $product['package_quantity'], 'Package Quantity', true);
                $data = array(
                    'Package Quantity' => array(
                        'name' => 'Package Quantity',
                        'value' => '',
                        'is_visible' => '1',
                        'is_variation' => '1',
                        'is_taxonomy' => '1'
                    )
                );
                $_product_attributes = get_post_meta($productID, '_product_attributes', true);
                update_post_meta($productID, '_product_attributes', array_merge($_product_attributes, $data));

                $counter++;

                echo 'Products uploaded: '.$counter.'/'.$productsNum.PHP_EOL;

            }
        }
    }

    public function uploadMedia($image_url){
        require_once(ABSPATH .'wp-admin/includes/image.php');
        require_once(ABSPATH .'wp-admin/includes/file.php');
        require_once(ABSPATH .'wp-admin/includes/media.php');
        $media = media_sideload_image($image_url,0);
        $attachments = get_posts(array(
            'post_type' => 'attachment',
            'post_status' => null,
            'post_parent' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC'
        ));
        return $attachments[0]->ID;
    }

}

$parser = new StoreParser('https://ua.iherb.com/');
$products = $parser->parse();
$parser->uploadProducts($products);